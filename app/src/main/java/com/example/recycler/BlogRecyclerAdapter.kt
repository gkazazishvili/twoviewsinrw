package com.example.recycler

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

class BlogRecyclerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var items: ArrayList<BlogPost> = ArrayList()


    override fun getItemViewType(position: Int): Int {
        if(items.get(position).title.length > 4){
            return 0
        } else {
            return 1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):RecyclerView.ViewHolder {
        return if (viewType == 0) {
            BlogViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.layout_blog_list_item, parent, false)
            )
        }
        else {
            SecondBlogViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.second_layout_blog_list_item, parent, false)
            )
        }
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when(holder){
            is BlogViewHolder ->{
                holder.bind(position)
                holder.dlt(position)
                holder.update.setOnClickListener {
                    holder.update(position)

                }

            }
            is SecondBlogViewHolder -> {
                holder.bind(position)
                holder.dlt2(position)
                holder.update.setOnClickListener {
                    holder.update(position)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class BlogViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        private val blogTitle: TextView = itemView.findViewById<TextView>(R.id.blog_title)
        private val dltBtn: Button = itemView.findViewById<Button>(R.id.dltBtn)
        val update: Button = itemView.findViewById<Button>(R.id.updBtn)
        private val etUpd:EditText? = itemView.findViewById<EditText>(R.id.updateThis)

        fun bind(position: Int) {

            blogTitle.text = items.get(position).title.toString()
        }

        fun dlt(index: Int){
            dltBtn.setOnClickListener {
                deleteItem(index)
            }
        }

        fun update(position: Int){
                items[position].title = etUpd?.text.toString()
            etUpd?.text?.clear()
            notifyDataSetChanged()
        }
    }


    inner class SecondBlogViewHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {
        private val blogTitle: TextView = itemView.findViewById<TextView>(R.id.blog_title)
        private val dltBtn: Button = itemView.findViewById<Button>(R.id.dltBtn)
        private val etUpd:EditText? = itemView.findViewById<EditText>(R.id.updateThis)
        val update: Button = itemView.findViewById<Button>(R.id.updBtn)




        fun bind(position:Int) {
            blogTitle.text = items.get(position).title.toString()

        }

        fun dlt2(index: Int){
            dltBtn.setOnClickListener {
                deleteItem(index)
            }
        }

        fun update(position: Int){
                    items[position].title = etUpd?.text.toString()
                etUpd?.text?.clear()
                    notifyDataSetChanged()
        }



    }
    fun deleteItem(index: Int){
        items.removeAt(index)
        notifyDataSetChanged()
    }
}
