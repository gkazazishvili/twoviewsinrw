package com.example.recycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recycler.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var blogRecyclerAdapter: BlogRecyclerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        init()

    }

    private fun init(){
        listener()
        val recycler:RecyclerView = findViewById<RecyclerView>(R.id.recycler_view)
        recycler.layoutManager = GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false)
        blogRecyclerAdapter = BlogRecyclerAdapter()
        recycler.adapter = blogRecyclerAdapter
    }

    private fun listener(){
        binding.add.setOnClickListener {
            blogRecyclerAdapter.items.add(BlogPost(binding.etMainAdd.text.toString()))
            blogRecyclerAdapter.notifyDataSetChanged()
        }
    }
}